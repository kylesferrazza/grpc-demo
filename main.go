package main

import "fmt"

func main() {
	fmt.Println("Run the server or client with `go run ./cmd/server` or `go run ./cmd/client`")
}
