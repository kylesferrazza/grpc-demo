package logging

import (
	"net/http"
	"os"

	"github.com/sirupsen/logrus"
)

func SetupLogger() {
	logrus.SetFormatter(&logrus.TextFormatter{
		FullTimestamp: true,
		PadLevelText:  true,
	})
	logrus.SetLevel(logrus.InfoLevel)
	_, debugSet := os.LookupEnv("DEBUG")
	if debugSet {
		logrus.SetLevel(logrus.DebugLevel)
	}
}

func LogRequests(origHandler http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		logrus.WithFields(logrus.Fields{
			"RemoteAddr": r.RemoteAddr,
			"URL":        r.URL.String(),
		}).Info("got HTTP request")
		origHandler(w, r)
	}
}
