package rpc

import (
	"net/http"
	"strings"

	"github.com/sirupsen/logrus"
	"golang.org/x/net/http2"
	"golang.org/x/net/http2/h2c"
	"google.golang.org/grpc"
)

func SplitHandler(grpcServer *grpc.Server, httpHandler http.Handler) http.Handler {
	return h2c.NewHandler(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.ProtoMajor == 2 && strings.Contains(r.Header.Get("Content-Type"), "application/grpc") {
			logrus.Debug("handling as grpc")
			grpcServer.ServeHTTP(w, r)
		} else {
			logrus.Debug("handling as http")
			httpHandler.ServeHTTP(w, r)
		}
	}), &http2.Server{})
}
