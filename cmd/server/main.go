package main

import (
	"context"
	"fmt"
	"net/http"

	"github.com/sirupsen/logrus"
	"gitlab.com/kylesferrazza/grpc-channel-demo/lib/logging"
	"gitlab.com/kylesferrazza/grpc-channel-demo/lib/rpc"
	"gitlab.com/kylesferrazza/grpc-channel-demo/lib/rpc/proto"
	"google.golang.org/grpc"
)

type DemoServer struct {
	proto.UnimplementedDemoServer
}

var lastName = "[none received yet]"

func (ds *DemoServer) ClientHello(ctx context.Context, req *proto.HelloRequest) (*proto.HelloReply, error) {
	logrus.WithFields(logrus.Fields{
		"Name": req.Name,
	}).Infoln("Received client hello")
	lastName = req.Name
	return &proto.HelloReply{
		Message: fmt.Sprintf("Hello, %s!", req.Name),
	}, nil
}

func main() {
	logging.SetupLogger()
	logrus.Infoln("Starting server.")

	grpcServer := grpc.NewServer()
	proto.RegisterDemoServer(grpcServer, &DemoServer{})

	http.HandleFunc("/", logging.LogRequests(indexHandler))

	webserver := http.Server{
		Addr:    "127.0.0.1:8081",
		Handler: rpc.SplitHandler(grpcServer, http.DefaultServeMux),
	}

	webserver.ListenAndServe()
}

func indexHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Last name from client: %s", lastName)
}
