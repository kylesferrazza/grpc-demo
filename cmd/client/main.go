package main

import (
	"context"
	"fmt"
	"io"
	"net/http"
	"time"

	"github.com/sirupsen/logrus"
	"gitlab.com/kylesferrazza/grpc-channel-demo/lib/logging"
	"gitlab.com/kylesferrazza/grpc-channel-demo/lib/rpc/proto"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

func main() {
	grpcConn, err := grpc.Dial("127.0.0.1:8081", grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		logrus.WithError(err).Fatalln("Error setting up grpc connection. Is the server running?")
	}
	defer grpcConn.Close()
	logging.SetupLogger()
	logrus.Infoln("Starting client.")

	http.HandleFunc("/", logging.LogRequests(createIndexHandler(grpcConn)))

	webserver := http.Server{
		Addr:    "127.0.0.1:8080",
		Handler: http.DefaultServeMux,
	}

	webserver.ListenAndServe()
}

func sendReq(grpcConn *grpc.ClientConn, name string) (string, error) {
	c := proto.NewDemoClient(grpcConn)

	// Contact the server and print out its response.
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()
	r, err := c.ClientHello(ctx, &proto.HelloRequest{Name: name})
	if err != nil {
		return "", err
	}
	return r.Message, nil
}

func createIndexHandler(grpcConn *grpc.ClientConn) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		query := r.URL.Query()
		if query.Has("name") {
			name := query.Get("name")
			fmt.Fprintf(w, "Sent gRPC hello to server with name [%s]\n", name)
			response, err := sendReq(grpcConn, name)
			if err == nil {
				fmt.Fprintf(w, "response: [%s]\n", response)
			} else {
				fmt.Fprintf(w, "error from server: [%v]\n", err)
			}
		} else {
			io.WriteString(w, "Set the 'name' query parameter to send a grpc message to the server.")
		}
	}
}
