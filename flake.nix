{
  description = "grpc-channel-demo";

  outputs = { self, nixpkgs }: let
    pkgs = import nixpkgs {
      system = "x86_64-linux";
    };
  in {
    devShell.x86_64-linux = pkgs.mkShell {
      name = "grpc-channel-demo";
      buildInputs = with pkgs; [
        go
        gopls
        protobuf
      ];
    };
  };
}
